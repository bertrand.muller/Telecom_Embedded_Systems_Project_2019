#include "pch.h"
#include "Sensor.h"

// Define PI value
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using namespace Platform;

Sensor::Sensor(Platform::String^ id, String^ color, double temperature, double latitude, double longitude) {
	this->id = id;
	this->color = color;
	this->temperature = temperature;
	this->location = ref new Location(latitude, longitude);
}

Platform::String^ Sensor::getId() {
	return this->id;
}

Location^ Sensor::getLocation() {
	return this->location;
}

double Sensor::getLatitude() {
	return this->location->getLatitude();
}

double Sensor::getLongitude() {
	return this->location->getLongitude();
}

double Sensor::getTemperature() {
	return this->temperature;
}

String^ Sensor::getColor() {
	return this->color;
}

void Sensor::setLatitude(double latitude) {
	this->location->setLatitude(latitude);
}

void Sensor::setLongitude(double longitude) {
	this->location->setLatitude(longitude);
}

void Sensor::setTemperature(double temperature) {
	this->temperature = temperature;
}

void Sensor::setColor(String^ color) {
	this->color = color;
}

double Sensor::distanceInMetersFromGPSPosition(double lat, double lon) {

	/*
	 * Use the "haversine" formula
	 * Calculate distance between two GPS points (shortest distance over the earth's surface)
	 * Ignore hills, mountains, holes, ...
	 * Method : Spherical Law of Cosines
	 * Website : https://www.movable-type.co.uk/scripts/latlong.html
	 */

	 // Convert latitudes & longitudes to radians
	double lat1  = this->location->getLatitude() * M_PI / 180;
	double long1 = this->location->getLongitude() * M_PI / 180;
	double lat2  = lat * M_PI / 180;
	double long2 = lon * M_PI / 180;

	// Calculate distance
	double R = 6371000; // Earth radius in meters
	double distance = R * acos(cos(lat1) * cos(lat2) * cos(long2 - long1) + sin(lat1) * sin(lat2));

	return distance;

}