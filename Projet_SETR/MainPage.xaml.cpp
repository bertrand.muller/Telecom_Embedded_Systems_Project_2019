﻿//
// MainPage.xaml.cpp
// Implémentation de la classe MainPage.
//

#include "pch.h"
#include "MainPage.xaml.h"
#include "Views/HomeView.xaml.h"
#include "Views/MapMarkersView.xaml.h"
#include "Views/AllMotesView.xaml.h"
#include <string>
#include <Sensor.h>
#include <Toolbox.h>
#include <Location.h>

// Define PI value
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

using namespace Projet_SETR;

using namespace Platform;
using namespace Windows::Data::Json;
using namespace Windows::Devices::Geolocation;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::System::Threading;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::Web::Http;

using namespace concurrency;

DWORD GeolocationThreadID;
HANDLE GeolocationThread;
DWORD WINAPI GeolocationThreadFunction(LPVOID param);

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

MainPage::MainPage() {
	InitializeComponent();
	InitializeNavigationView();
	GetSensorsPositions();
	GeolocationThread = CreateThread(NULL, 0, GeolocationThreadFunction, (void*)this, 0, &GeolocationThreadID);
	InitializeTimer();
}


void Projet_SETR::MainPage::InitializeNavigationView() {
	NavView->IsBackEnabled = false;
	NavView->IsBackButtonVisible = NavigationViewBackButtonVisible(false);
	NavView->IsSettingsVisible = false;
	NavView->IsPaneOpen = false;
	ContentFrame->Navigate(TypeName(HomeView::typeid));
}


void Projet_SETR::MainPage::InitializeTimer() {

	TimeSpan period;
	period.Duration = 2 * 10000000; // Function executed every two seconds

	ThreadPoolTimer^ PeriodicTimer = ThreadPoolTimer::CreatePeriodicTimer(
		ref new TimerElapsedHandler([this](ThreadPoolTimer^ source) {
			Dispatcher->RunAsync(CoreDispatcherPriority::High, ref new DispatchedHandler([this]() {
				RecalculateClosestSensor();
			}));
		}),
		period
	);

}

void Projet_SETR::MainPage::RecalculateClosestSensor() {

	double uLatitude = GetUserLatitude();
	double uLongitude = GetUserLongitude();
	std::vector<Sensor^> allSensors = GetAllSensors();

	if ((uLatitude != NULL) && (uLongitude != NULL) && (allSensors.size() > 0)) {

		// Calculate the minimum distance between mobile phone and a mote
		double minDistance = allSensors[0]->distanceInMetersFromGPSPosition(uLatitude, uLongitude);
		Sensor^ closestSensor = allSensors[0];
		for (auto i = 1; i < allSensors.size(); i++) {
			double distance = allSensors[i]->distanceInMetersFromGPSPosition(uLatitude, uLongitude);
			if (minDistance > distance) {
				minDistance = distance;
				closestSensor = allSensors[i];
			}
		}

		// Save closest sensor
		SetClosestSensor(closestSensor);

		// Get temperature of the closest sensor
		RetrieveClosestSensorTemperature(closestSensor);

	}

}


void Projet_SETR::MainPage::GetSensorsPositions() {

	std::vector<String^> urls = { "http://iotlab.telecomnancy.eu:8080/iotlab/rest/info/motes", "https://kyominii.com/motes.json" };
	Uri^ requestURL = ref new Uri(urls[1]);
	HttpClient^ httpClient = ref new HttpClient();

	IAsyncOperationWithProgress<String^, HttpProgress>^ getOperation = httpClient->GetStringAsync(requestURL);
	auto request = create_task(getOperation);

	request.then([](task<String^> response) {

		auto motes = response.get();
		JsonObject^ tokenResponse = ref new JsonObject();

		if (JsonObject::TryParse(response.get(), &tokenResponse)) {

			auto map = tokenResponse->GetView();
			IJsonValue^ value = map->Lookup("sender");

			String^ s = value->Stringify();
			JsonArray^ mapValue = ref new JsonArray();

			if (JsonArray::TryParse(s, &mapValue)) {

				// Loop over all motes returned from HTTP request
				auto vec = mapValue->GetView();
				std::for_each(begin(vec), end(vec), [](IJsonValue^ item) {
					String^ id = item->GetObject()->GetNamedString("mac");
					String^ latitude = (item->GetObject()->GetNamedNumber("lat")).ToString();
					String^ longitude = (item->GetObject()->GetNamedNumber("lon")).ToString();
					double latitudeDouble = _wtof(latitude->Data());
					double longitudeDouble = _wtof(longitude->Data());
					AddSensor(ref new Sensor(id, "orange", 0.0, latitudeDouble, longitudeDouble));
				});

			}

		}

	});

}


void Projet_SETR::MainPage::RetrieveClosestSensorTemperature(Sensor^ sensor) {

	String^ url = "http://iotlab.telecomnancy.eu:8080/iotlab/rest/data/1/temperature/last/" + sensor->getId();
	Uri^ requestURL = ref new Uri(url);
	HttpClient^ httpClient = ref new HttpClient();

	IAsyncOperationWithProgress<String^, HttpProgress>^ getOperation = httpClient->GetStringAsync(requestURL);
	auto request = create_task(getOperation);

	request.then([this, sensor](task<String^> response) {

		auto mote = response.get();
		JsonObject^ tokenResponse = ref new JsonObject();

		if (JsonObject::TryParse(response.get(), &tokenResponse)) {

			auto map = tokenResponse->GetView();
			IJsonValue^ value = map->Lookup("data");

			String^ s = value->Stringify();
			JsonArray^ mapValue = ref new JsonArray();

			if (JsonArray::TryParse(s, &mapValue)) {

				// Get temperature of closest sensor
				auto vec = mapValue->GetView();
				if (vec->Size > 0) {
					double temperature = vec->GetAt(0)->GetObject()->GetNamedNumber("value");
					sensor->setTemperature(temperature);
					SetIsInitializedClosestSensor(true);
				}

			}

		}

	});
}


DWORD WINAPI GeolocationThreadFunction(LPVOID param) {

	while (1) {

		task<GeolocationAccessStatus> geolocationAccessRequestTask(Geolocator::RequestAccessAsync());
		geolocationAccessRequestTask.then([](task<GeolocationAccessStatus> accessStatusTask) {

			auto accessStatus = accessStatusTask.get();
			if (accessStatus == GeolocationAccessStatus::Allowed) {
				auto geolocator = ref new Geolocator();

				task<Geoposition^> geopositionTask(geolocator->GetGeopositionAsync());
				geopositionTask.then([](task<Geoposition^> getPosTask) {

					Geoposition^ res = getPosTask.get();
					SetUserLatitude(res->Coordinate->Latitude);
					SetUserLongitude(res->Coordinate->Longitude);
					OutputMessage("Location = (latitude: " + GetUserLatitude() + " - longitude : " + GetUserLongitude() + ")");

				});

			}

		});

		Sleep(5000);

	}

	return 0;

}


void Projet_SETR::MainPage::NavView_ItemInvoked(NavigationView^ sender, NavigationViewItemInvokedEventArgs^ args) {

	String^ page = args->InvokedItem->ToString();
	String^ locations = "";
	double uLatitude = GetUserLatitude();
	double uLongitude = GetUserLongitude();
	boolean isInitializedClosestSensor = IsInitializedClosestSensor();
	std::vector<Sensor^> allSensors = GetAllSensors();

	if (page == "Accueil" || page == "Le plus proche") {
		ContentFrame->Navigate(TypeName(HomeView::typeid), locations);
	} else if (page == "Ma position") {

		if ((isInitializedClosestSensor) && (uLatitude != NULL) && (uLongitude != NULL)) {
			locations += "Ma position;16;+";
			locations += uLatitude + ";" + uLongitude;
			ContentFrame->Navigate(TypeName(MapMarkersView::typeid), locations);
		}

	} else if (page == "Capteurs") {

		if ((isInitializedClosestSensor) && (allSensors.size() > 0) && (uLatitude != NULL) && (uLongitude != NULL)) {

			// Options
			locations += "Capteur le plus proche;18+";

			// Add closest sensor first
			double closestSensorLatitude = GetClosestSensorLatitude();
			double closestSensorLongitude = GetClosestSensorLongitude();
			locations += closestSensorLatitude + ";" + closestSensorLongitude;

			// Add current position
			locations += "|" + uLatitude + ";" + uLongitude;

			// Add other sensors
			for (int i = 0; i < allSensors.size(); i++) {
				double sensorLatitude = allSensors[i]->getLatitude();
				double sensorLongitude = allSensors[i]->getLongitude();
				if ((closestSensorLatitude != sensorLatitude) && (closestSensorLongitude != sensorLongitude)) {
					locations += "|" + sensorLatitude + ";" + sensorLongitude;
				}
			}

			ContentFrame->Navigate(TypeName(MapMarkersView::typeid), locations);

		}

	} else if (page == "Itinéraire") {

		if ((isInitializedClosestSensor) && (uLatitude != NULL) && (uLongitude != NULL)) {
			//auto uriNetwork = ref new Uri("bingmaps:?rtp=adr.Mountain%20View,%20CA~adr.San%20Francisco%20International%20Airport,%20CA&mode=w");
			auto uriNetwork = ref new Uri("bingmaps:?rtp=pos." + GetUserLatitude() + "_" + GetUserLongitude() + "_Ma%20position~pos." + GetClosestSensorLatitude() + "_" + GetClosestSensorLongitude() + "_Capteur%20n°" + GetClosestSensorId() + "&mode=w");
			auto launcherOptions = ref new Windows::System::LauncherOptions();
			auto success = Windows::System::Launcher::LaunchUriAsync(uriNetwork, launcherOptions);
		}

	} else if (page == "Tous") {

		if (isInitializedClosestSensor) {
			ContentFrame->Navigate(TypeName(AllMotesView::typeid));
		}

	}

}
