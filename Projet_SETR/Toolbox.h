#pragma once
#include <Sensor.h>

using namespace Platform;

#ifndef MY_TOOLBOX_HEADER_
	#define MY_TOOLBOX_HEADER_

	void OutputMessage(Object^ parameter);

	boolean IsInitializedClosestSensor();
	std::vector<Sensor^> GetAllSensors();
	double GetUserLatitude();
	double GetUserLongitude();
	String^ GetClosestSensorId();
	double GetClosestSensorTemperature();
	double GetClosestSensorLatitude();
	double GetClosestSensorLongitude();
	double GetClosestSensorDistance();

	void AddSensor(Sensor^ s);
	void SetIsInitializedClosestSensor(boolean isInit);
	void SetUserLatitude(double userLat);
	void SetUserLongitude(double userLongitude);
	void SetClosestSensor(Sensor^ s);

#endif
