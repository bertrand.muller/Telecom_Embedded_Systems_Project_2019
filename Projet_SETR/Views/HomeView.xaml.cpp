﻿//
// Home.xaml.cpp
// Implementation of the Home class
//

#include "pch.h"
#include "HomeView.xaml.h"
#include "Toolbox.h"
#include <sstream>

using namespace Projet_SETR;

using namespace Platform;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::System::Threading;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

using namespace concurrency;

String^ sensorId;
double sensorTemperature, sensorLatitude, sensorLongitude;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

HomeView::HomeView() {
	InitializeComponent();
	InitializeTimer();
}


void Projet_SETR::HomeView::InitializeTimer() {

	TimeSpan period;
	period.Duration = 2 * 10000000; // Function executed every two seconds

	ThreadPoolTimer^ PeriodicTimer = ThreadPoolTimer::CreatePeriodicTimer(
		ref new TimerElapsedHandler([this](ThreadPoolTimer^ source) {
			Dispatcher->RunAsync(CoreDispatcherPriority::High, ref new DispatchedHandler([this]() {
				RefreshView();
			}));
		}),
		period
	);

}


void Projet_SETR::HomeView::RefreshView() {

	// User latitude
	if (GetUserLatitude() != 0) {
		userLatitudeValue->Text = GetUserLatitude().ToString();
	} else {
		userLatitudeValue->Text = "?";
	}
	
	// User longitude
	if (GetUserLongitude() != 0) {
		userLongitudeValue->Text = GetUserLongitude().ToString();
	} else {
		userLongitudeValue->Text = "?";
	}

	// Closest sensor
	if (IsInitializedClosestSensor()) {
		closestSensorLatitudeValue->Text = GetClosestSensorLatitude().ToString();
		closestSensorLongitudeValue->Text = GetClosestSensorLongitude().ToString();
		closestSensorIdValue->Text = GetClosestSensorId();
		closestSensorTemperatureValue->Text = GetClosestSensorTemperature() + "°C";
		closestSensorDistanceValue->Text = GetClosestSensorDistance().ToString() + " m";
	} else {
		closestSensorLatitudeValue->Text = "?";
		closestSensorLongitudeValue->Text = "?";
		closestSensorIdValue->Text = "";
		closestSensorTemperatureValue->Text = "?";
		closestSensorDistanceValue->Text = "?";
	}
	
}
