﻿//
// MapView.xaml.cpp
// Implementation of the MapView class
//

#include "pch.h"
#include "MapMarkersView.xaml.h"
#include <Toolbox.h>
#include <sstream>

using namespace Projet_SETR;

using namespace Platform;
using namespace Platform::Collections;
using namespace Windows::Devices::Geolocation;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Maps;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

std::vector<Location^> locations;
double zoomLevel;
String^ label;

MapMarkersView::MapMarkersView() {
	InitializeComponent();
}


void MapMarkersView::CenterMap(Location^ l) {

    double lat, lon;
    if (label == "Ma position") {
        lat = GetUserLatitude();
        lon = GetUserLongitude();
    } else {
        lat = GetClosestSensorLatitude();
        lon = GetClosestSensorLongitude();
    }

    // Center map on Telecom Nancy (48.669237;6.157741)
    BasicGeoposition* position = new BasicGeoposition();
    position->Latitude = lat;
    position->Longitude = lon;
    latitudeValue->Text = "Latitude | " + lat;
    longitudeValue->Text = "Longitude | " + lon;

    Geopoint^ point = ref new Geopoint(*position);
    myMap->Center = point;
    myMap->ZoomLevel = zoomLevel;

}


void MapMarkersView::CreatePointsOfInterest() {

    Vector<MapElement^>^ vec = ref new Vector<MapElement^>();
    
    int i = 0;
    for (auto l : locations) {

        BasicGeoposition* sensorPosition = new BasicGeoposition();
        sensorPosition->Latitude = l->getLatitude();
        sensorPosition->Longitude = l->getLongitude();
        Geopoint^ sensorGeopoint = ref new Geopoint(*sensorPosition);

        MapIcon^ icon = ref new MapIcon();
        icon->Location = sensorGeopoint;
        icon->NormalizedAnchorPoint = *(ref new Point(0.5, 1.0));
        icon->ZIndex = 0;

        if (i == 0) {
            icon->Title = label;
        } else {
            icon->Title = "";
        }

        vec->Append(icon);
        i++;

    }

    MapElementsLayer^ landmarksLayer = ref new MapElementsLayer();
    landmarksLayer->ZIndex = 1;
    landmarksLayer->MapElements = vec;

    myMap->Layers->Append(landmarksLayer);
    CenterMap(locations[0]);

}


std::vector<std::wstring> split(std::wstring strToSplit, wchar_t delimiter) {

    std::vector<std::wstring> splittedStrings;
    std::wstringstream wss(strToSplit);
    std::wstring item;

    while (std::getline(wss, item, delimiter)) {
        splittedStrings.push_back(item);
    }

    return splittedStrings;

}


void Projet_SETR::MapMarkersView::ClearMap() {
    myMap->Layers->Clear();
    locations.clear();
}

void Projet_SETR::MapMarkersView::OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) {

    // Force refreshing map when re-accessing map
    ClearMap();

    String^ stringifiedLocations = e->Parameter->ToString();

    /* BIG STRING */
    // Split according to '+' character
    std::wstring s1(stringifiedLocations->Begin(), stringifiedLocations->End());
    std::vector<std::wstring> splitBigString = split(s1, L'+');

    if (splitBigString.size() > 0) {

        /* OPTIONS */
        // Split according to ';' character
        std::vector<std::wstring> options = split(splitBigString[0], L';');

        if (options.size() > 0) {
            label = ref new String(options[0].c_str());
            zoomLevel = _wtof(options[1].c_str());
            OutputMessage(label);
            OutputMessage(zoomLevel);
            temperatureValue->Text = "Température | " + GetClosestSensorTemperature() + "°C";
        }


        /* GPS DATA */
        // Split according to '|' character
        std::vector<std::wstring> pairs = split(splitBigString[1], L'|');

        if (pairs.size() > 0) {

            // Split according to ';' character
            for (auto p : pairs) {
                std::vector<std::wstring> coordinates = split(p, L';');
                double latitude = _wtof(coordinates[0].c_str());
                double longitude = _wtof(coordinates[1].c_str());
                Location^ l = ref new Location(latitude, longitude);
                locations.push_back(l);
            }

            CreatePointsOfInterest();

        } else {
            latitudeValue->Text = "";
            longitudeValue->Text = "";
        }

    }

}
