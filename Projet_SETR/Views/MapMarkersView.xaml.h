﻿//
// Map.xaml.h
// Declaration of the Map class
//

#pragma once

#include "Views/MapMarkersView.g.h"
#include <Location.h>

namespace Projet_SETR
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class MapMarkersView sealed {
		public:
			MapMarkersView();

		private:
			void CenterMap(Location^ l);
			void CreatePointsOfInterest();
			void ClearMap();

		protected: 
			virtual void OnNavigatedTo(Windows::UI::Xaml::Navigation::NavigationEventArgs^ e) override;
	};
}
