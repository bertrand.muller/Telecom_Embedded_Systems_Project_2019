﻿//
// Home.xaml.h
// Declaration of the Home class
//

#pragma once

#include "Views/HomeView.g.h"

namespace Projet_SETR {

	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class HomeView sealed {

		public:
			HomeView();
			
		private:
			void InitializeTimer();
			void RefreshView();

	};
}
