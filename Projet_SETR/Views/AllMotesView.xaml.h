﻿//
// AllMotesView.xaml.h
// Declaration of the AllMotesView class
//

#pragma once

#include "Views/AllMotesView.g.h"

namespace Projet_SETR {

	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	[Windows::Foundation::Metadata::WebHostHidden]
	public ref class AllMotesView sealed {

		public:
			AllMotesView();

		private:
			void ListAllMotes();

	};
}
