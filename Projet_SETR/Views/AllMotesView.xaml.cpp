﻿//
// AllMotesView.xaml.cpp
// Implementation of the AllMotesView class
//

#include "pch.h"
#include "AllMotesView.xaml.h"
#include "Toolbox.h"

using namespace Projet_SETR;

using namespace Platform;
using namespace Windows::Data::Json;
using namespace Windows::Foundation;
using namespace Windows::Foundation::Collections;
using namespace Windows::System::Threading;
using namespace Windows::UI::Core;
using namespace Windows::UI::Xaml;
using namespace Windows::UI::Xaml::Controls;
using namespace Windows::UI::Xaml::Controls::Primitives;
using namespace Windows::UI::Xaml::Interop;
using namespace Windows::UI::Xaml::Data;
using namespace Windows::UI::Xaml::Input;
using namespace Windows::UI::Xaml::Media;
using namespace Windows::UI::Xaml::Navigation;
using namespace Windows::Web::Http;

using namespace concurrency;
using namespace std;

ItemCollection^ listItems;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

AllMotesView::AllMotesView() {
	InitializeComponent();
	ListAllMotes();
}


void AllMotesView::AllMotesView::ListAllMotes() {

	listItems = AllMotesList->Items;
	listItems->Clear();

	std::vector<Sensor^> allSensors = GetAllSensors();
	for (auto s : allSensors) {
		listItems->Append("Capteur n°" + s->getId() + "\r\n Chargement de la température... \r\n");
	}

	String^ url = "http://iotlab.telecomnancy.eu:8080/iotlab/rest/data/1/temperature/last/";
	Uri^ requestURL = ref new Uri(url);
	HttpClient^ httpClient = ref new HttpClient();

	IAsyncOperationWithProgress<String^, HttpProgress>^ getOperation = httpClient->GetStringAsync(requestURL);
	auto request = create_task(getOperation);

	request.then([](task<String^> response) {

		auto mote = response.get();
		JsonObject^ tokenResponse = ref new JsonObject();

		if (JsonObject::TryParse(response.get(), &tokenResponse)) {

			auto map = tokenResponse->GetView();
			IJsonValue^ value = map->Lookup("data");

			String^ s = value->Stringify();
			JsonArray^ mapValue = ref new JsonArray();

			if (JsonArray::TryParse(s, &mapValue)) {

				// Get temperature of closest sensor
				auto vec = mapValue->GetView();
				listItems->Clear();
				std::for_each(begin(vec), end(vec), [](IJsonValue^ item) {
					String^ id = item->GetObject()->GetNamedString("mote");
					double temperature = item->GetObject()->GetNamedNumber("value");
					listItems->Append("Capteur n°" + id + "\r\n" + temperature + "°C\r\n");
				});

			}

		}

	});

}
