#include "pch.h"
#include "Location.h"

Location::Location(double latitude, double longitude) {
	this->latitude = latitude;
	this->longitude = longitude;
}

double Location::getLatitude() {
	return this->latitude;
}

double Location::getLongitude() {
	return this->longitude;
}

void Location::setLatitude(double latitude) {
	this->latitude = latitude;
}

void Location::setLongitude(double longitude) {
	this->longitude = longitude;
}