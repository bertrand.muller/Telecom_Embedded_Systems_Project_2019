#include "pch.h"
#include "Toolbox.h"

std::vector<Sensor^> allSensors;
boolean isInitClosestSensor = false;
Sensor^ referenceToClosestSensor;

double userLatitude = NULL;
double userLongitude = NULL;


void OutputMessage(Object^ parameter) {
	auto parameterToString = parameter->ToString();
	auto formattedParameter = std::wstring(parameterToString->Data()).append(L"\r\n");
	OutputDebugString(formattedParameter.c_str());
}

boolean IsInitializedClosestSensor() {
	return isInitClosestSensor;
}

std::vector<Sensor^> GetAllSensors() {
	return allSensors;
}

double GetUserLatitude() {
	return userLatitude;
}

double GetUserLongitude() {
	return userLongitude;
}

String^ GetClosestSensorId() {
	return referenceToClosestSensor->getId();
}

double GetClosestSensorTemperature() {
	return referenceToClosestSensor->getTemperature();
}

double GetClosestSensorLatitude() {
	return referenceToClosestSensor->getLatitude();
}

double GetClosestSensorLongitude() {
	return referenceToClosestSensor->getLongitude();
}

double GetClosestSensorDistance() {
	return referenceToClosestSensor->distanceInMetersFromGPSPosition(GetUserLatitude(), GetUserLongitude());
}

void AddSensor(Sensor^ s) {
	allSensors.push_back(s);
}

void SetIsInitializedClosestSensor(boolean isInit) {
	isInitClosestSensor = isInit;
}

void SetUserLatitude(double userLat) {
	userLatitude = userLat;
}

void SetUserLongitude(double userLon) {
	userLongitude = userLon;
}

void SetClosestSensor(Sensor^ s) {
	referenceToClosestSensor = s;
}