#pragma once
#include "Location.h"

ref class Sensor sealed {

	public:
		Sensor(Platform::String^ id, Platform::String^ color, double temperature, double latitude, double longitude);

		Platform::String^ getId();
		Location^ getLocation();
		double getLatitude();
		double getLongitude();
		double getTemperature();
		Platform::String^ getColor();

		void setLatitude(double latitude);
		void setLongitude(double longitude);
		void setTemperature(double temperature);
		void setColor(Platform::String^ color);

		double distanceInMetersFromGPSPosition(double lat, double lon);
		
	private:
		Platform::String^ id;
		Location^ location;
		double temperature;
		Platform::String^ color;

};

