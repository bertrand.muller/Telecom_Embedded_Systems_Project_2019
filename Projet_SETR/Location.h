#pragma once

ref class Location sealed {

	public:
		Location(double latitude, double longitude);

		double getLatitude();
		double getLongitude();

		void setLatitude(double latitude);
		void setLongitude(double longitude);

	private:
		double latitude, longitude;

};

