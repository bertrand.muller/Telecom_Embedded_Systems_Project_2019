﻿//
// MainPage.xaml.h
// Déclaration de la classe MainPage.
//

#pragma once

#include "MainPage.g.h"
#include "Sensor.h"

using namespace Windows::UI::Xaml::Controls;

namespace Projet_SETR
{
	/// <summary>
	/// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
	/// </summary>
	public ref class MainPage sealed
	{
		public:
			MainPage();

		private:
			MainPage^ rootPage;

			void InitializeNavigationView();
			void InitializeTimer();
			void RecalculateClosestSensor();
			void GetSensorsPositions();
			void RetrieveClosestSensorTemperature(Sensor^ sensor);
			void NavView_ItemInvoked(NavigationView^ sender, NavigationViewItemInvokedEventArgs^ args);

	};
}
